﻿Shader "Custom/Sea"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _BumpTex ("Normal (Height)", 2D) = "white" {}
        _Color ("Color", color) = (1, 1, 1, 1)
        _Shininess ("Shininess", Range(0, 1)) = 0.5
        _SpecColor ("Spec color", color) = (0.5, 0.5, 0.5, 0.5)
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        
        CGPROGRAM
        #pragma surface surf BlinnPhongOneSided alpha
        #pragma target 3.0

        #include "../CustomLighting.cginc"

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpTex;
            float3 worldPos;
            float3 viewDir;
        };
        
        sampler2D _MainTex;
        float4 _MainTex_ST;
        sampler2D _BumpTex;
        float4 _BumpTex_ST;
        fixed4 _Color;
        half _Shininess;

        void surf(Input IN, inout SurfaceOutput o)
        {
            half4 c = tex2D(_MainTex, IN.worldPos.xz * 0.001f * _MainTex_ST.xy + _MainTex_ST.zw);
            o.Albedo = c.rgb * _Color.rgb;
            o.Specular = _Shininess;
            o.Normal = UnpackNormal(tex2D(_BumpTex, IN.worldPos.xz * 0.001f * _BumpTex_ST.xy + _BumpTex_ST.zw));
            half rim = 1.5 - saturate(dot (normalize(IN.viewDir), o.Normal));
            o.Alpha = c.a * _Color.a * rim * rim; // TODO alpha is lower when far from the player (no more fog nedded)
            o.Gloss = o.Alpha;
        }
        ENDCG
    } 

    //FallBack "Diffuse"
}

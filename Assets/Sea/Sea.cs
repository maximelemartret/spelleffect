﻿using UnityEngine;
using System.Collections.Generic;

public class Sea : MonoBehaviour
{
    public float SeaLevel = 50.0f;

    protected Vector3 pOffset;
    protected MeshRenderer pMeshRenderer;
    protected static float pSeaLevel = 50.0f;

    public static float GetSeaLevel()
    {
        return pSeaLevel;
    }

    void Start()
    {
        pMeshRenderer = GetComponent<MeshRenderer>();
        pOffset = new Vector3(transform.position.x, 0, transform.position.z);
    }
    
    void Update()
    {
        if (SeaLevel != pSeaLevel)
        {
            pSeaLevel = SeaLevel;
        }

        Transform targetTransform = Camera.main.transform;
        float seaHighWave = Mathf.Cos(Time.time) * 0.4f;

        transform.position = new Vector3(targetTransform.position.x, SeaLevel + seaHighWave, targetTransform.position.z);
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, targetTransform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
        transform.Translate(pOffset, Space.Self);

        Vector2 seaPlaneWave = new Vector2(Mathf.Cos(Time.time), Mathf.Sin(Time.time) * 0.5f) * 0.01f;
        pMeshRenderer.materials[0].SetTextureOffset("_MainTex", seaPlaneWave);
        pMeshRenderer.materials[0].SetTextureOffset("_DispTex", seaPlaneWave);
        pMeshRenderer.materials[0].SetTextureOffset("_BumpTex", seaPlaneWave);
    }
}

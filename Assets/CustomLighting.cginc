#ifndef CUSTOM_LIGHTING_INCLUDED
#define CUSTOM_LIGHTING_INCLUDED

// NOTE: some intricacy in shader compiler on some GLES2.0 platforms (iOS) needs 'viewDir' & 'h'
// to be mediump instead of lowp, otherwise specular highlight becomes too bright.
inline fixed4 LightingBlinnPhongOneSided(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
{
    half3 h = normalize(lightDir + viewDir);

    fixed diff = max(0, dot(s.Normal, lightDir));

    float nh = max(0, dot(s.Normal, h));
    float spec = pow(nh, s.Specular * 128.0) * s.Gloss * ceil(diff);

    fixed4 c;
    c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * _SpecColor.rgb * spec) * (atten * 2);
    c.a = s.Alpha + _LightColor0.a * _SpecColor.a * spec * atten;
    return c;
}

inline fixed4 LightingBlinnPhongOneSided_PrePass(SurfaceOutput s, half4 light)
{
    fixed spec = light.a * s.Gloss;

    fixed4 c;
    c.rgb = (s.Albedo * light.rgb + light.rgb * _SpecColor.rgb * spec);
    c.a = s.Alpha + spec * _SpecColor.a;
    return c;
}

inline half4 LightingBlinnPhongOneSided_DirLightmap(SurfaceOutput s, fixed4 color, fixed4 scale, half3 viewDir, bool surfFuncWritesNormal, out half3 specColor)
{
    UNITY_DIRBASIS
    half3 scalePerBasisVector;

    half3 lm = DirLightmapDiffuse(unity_DirBasis, color, scale, s.Normal, surfFuncWritesNormal, scalePerBasisVector);

    half3 lightDir = normalize(scalePerBasisVector.x * unity_DirBasis[0] + scalePerBasisVector.y * unity_DirBasis[1] + scalePerBasisVector.z * unity_DirBasis[2]);
    half3 h = normalize(lightDir + viewDir);

    float nh = max(0, dot(s.Normal, h));
    float spec = pow(nh, s.Specular * 128.0);

    // specColor used outside in the forward path, compiled out in prepass
    specColor = lm * _SpecColor.rgb * s.Gloss * spec;

    // spec from the alpha component is used to calculate specular
    // in the Lighting*_Prepass function, it's not used in forward
    return half4(lm, spec);
}

#endif
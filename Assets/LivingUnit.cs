﻿using UnityEngine;
using System.Collections;

public class LivingUnit : MonoBehaviour {

	public int hpMax;
	public int mpMax;
	public int hp;
	public int mp;
	public int def;
	public int atk;
	public double speed;


	// Use this for initialization
	void Start ()
	{
		hpMax = 300;
		mpMax = 150;

		hp = 300;
		mp = 150;

		def = 10;
		atk = 25;

		speed = 1.5;
	}

	public void Hit (int modif)
	{
		hp -= modif;
		if (hp <= 0)
			Destroy (gameObject);
		Debug.Log("Hit! pv is now " + hp);
	}

	public void Heal (int modif)
	{
		hp += modif;
		if (hp >= hpMax)
			hp = hpMax;
	}

	// Update is called once per frame
	void Update ()
	{
		if (hp <= 0)
			Destroy (gameObject);
		if (hp >= hpMax)
			hp = hpMax;
		if (mp >= mpMax)
			mp = mpMax;
	}
}

﻿using UnityEngine;
using System.Collections;

public class Burn : AEffect {

	bool first;

	// Use this for initialization
	void Start ()
	{
		duration = 15.0f;
		tick = 1.5f;
		tickable = true;
		chrono = 0.0f;
		first = true;
	}

	protected override void OnApply()
	{
		Debug.Log ("Effect applied : Burn.");
		first = false;
	}

	protected override void OnDispell()
	{
		Debug.Log ("Effect removed : Burn.");
	}

	protected override void OnTick()
	{
		GetComponent<LivingUnit>().Hit(15);
	}

	void onDestroy()
	{
		OnDispell ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (first)
			OnApply ();
		OnUpdate ();
	}
}

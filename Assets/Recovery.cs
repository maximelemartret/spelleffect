﻿using UnityEngine;
using System.Collections;

public class Recovery : AEffect {

	// Use this for initialization
	void Start ()
	{
		duration = 21.0f;
		tick = 3.0f;
		tickable = true;
		chrono = 0.0f;
		OnApply ();
	}
	
	protected override void OnApply()
	{
		Debug.Log ("Effect applied : Recovery.");
	}
	
	protected override void OnDispell()
	{
		Debug.Log ("Effect removed : Recovery.");
	}
	
	protected override void OnTick()
	{
		GetComponent<LivingUnit>().Heal(25);
	}
	
	void onDestroy()
	{
		OnDispell ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		OnUpdate ();
	}
}

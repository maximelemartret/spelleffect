﻿using UnityEngine;
using System.Collections.Generic;

public class CustomTerrain : MonoBehaviour
{
    public int TerrainSize = 512;
    public int HeightmapSize = 512;
    public int AlphamapSize = 512;
    public int MapAlphamaps = 32;

    protected MeshFilter pFilter;
    protected MeshRenderer pRenderer;

    void Start()
    {
        pFilter = GetComponent<MeshFilter>();
        if (pFilter == null)
        {
            pFilter = gameObject.AddComponent<MeshFilter>();
        }

        pRenderer = GetComponent<MeshRenderer>();
        if (pRenderer == null)
        {
            pRenderer = gameObject.AddComponent<MeshRenderer>();
        }

        RecalculateMesh();
    }

    public void RecalculateMesh()
    {
        pFilter.mesh.Clear();

        Vector3[] vertices = new Vector3[TerrainSize * TerrainSize];
        Vector2[] uvs = new Vector2[vertices.Length];
        int[] indices;
        Vector3[] normals = new Vector3[vertices.Length];
        // Vector4[] tangents; // Really needed?

        
    }

    public void SetHeights(float u, float v, float value)
    {

    }

    public void SetAlphamaps(float u, float v, float[] values)
    {

    }
}

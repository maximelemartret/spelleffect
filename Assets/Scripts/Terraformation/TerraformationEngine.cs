﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

public class TerraformationEngine : MonoBehaviour
{
    public float CliffAngle = 25.0f;
    public int SandTextureId = 0;
    public int CliffTextureId = 1;
    public int GrassTextureId = 2;
    public float GrassGrowSpeed = 0.5f;

    protected bool pIsDone;

    protected struct pTerraformData
    {
        public Terrain terrain;
        public Texture2D mask;
        public Vector2 finalMaskSize;
        public Vector2 finalXZPosition;
        public float realPowerRatio;
    };

    protected delegate void TerraformAction(pTerraformData data, ref float[,] newHeights, ref float[,,] newAlphamaps);

    void Start()
    {
        pIsDone = false;

        foreach (Terrain terrain in Terrain.activeTerrains)
        {
            int alphaWidth = terrain.terrainData.alphamapWidth;
            int alphaHeight = terrain.terrainData.alphamapHeight;
            float[,,] alphamaps = terrain.terrainData.GetAlphamaps(0, 0, alphaWidth, alphaHeight);
            
            for (int y = 0; y < alphaHeight; y++)
            {
                for (int x = 0; x < alphaWidth; x++)
                {
                    pTerraformData data = new pTerraformData();
                    data.finalXZPosition = Vector3.zero;
                    data.terrain = terrain;

                    pUpdateAlphamaps(data, x, y, ref alphamaps);
                }
            }

            terrain.terrainData.SetAlphamaps(0, 0, alphamaps);
        }
    }

    //private int currentPart = 0;
    //private int partsCountSqrt = 8;
    void Update()
    {
        /*
        Terrain terrain = Terrain.activeTerrain;
        TerrainData data = terrain.terrainData;

        int alphaWidth = data.alphamapWidth;
        int alphaHeight = data.alphamapHeight;
        int width = alphaWidth / partsCountSqrt;
        int height = alphaHeight / partsCountSqrt;
        int idx = currentPart * width;
        int beginX = (idx % alphaWidth);
        int beginY = (idx / alphaWidth) * height;
        float[,,] alphamaps = data.GetAlphamaps(beginX, beginY, width, height);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float terrainHeight = terrain.terrainData.GetHeight(x, y);
                float angle = terrain.terrainData.GetSteepness((beginY + y) / alphaHeight, (beginX + x) / alphaWidth);

                if (angle < 45.0f)
                {
                    float blend = (1.0f - alphamaps[y, x, CliffTextureId]) * GrassGrowSpeed * Time.deltaTime;

                    if (terrainHeight * terrain.terrainData.size.y > Sea.GetSeaLevel() + 10.0f)
                    {
                        alphamaps[y, x, CliffTextureId] = Mathf.Lerp(alphamaps[y, x, CliffTextureId], 0.0f, blend);
                        alphamaps[y, x, SandTextureId] = Mathf.Lerp(alphamaps[y, x, SandTextureId], 0.0f, blend);
                        alphamaps[y, x, GrassTextureId] = 1.0f - (alphamaps[y, x, SandTextureId] + alphamaps[y, x, CliffTextureId]);
                    }
                    else if (terrainHeight * terrain.terrainData.size.y > Sea.GetSeaLevel() + 10.0f)
                    {
                        alphamaps[y, x, CliffTextureId] = Mathf.Lerp(alphamaps[y, x, CliffTextureId], 0.0f, blend);
                        alphamaps[y, x, GrassTextureId] = Mathf.Lerp(alphamaps[y, x, SandTextureId], 0.0f, blend);
                        alphamaps[y, x, SandTextureId] = 1.0f - (alphamaps[y, x, SandTextureId] + alphamaps[y, x, CliffTextureId]);
                    }
                }
            }
        }

        data.SetAlphamaps(beginX, beginY, alphamaps);
        currentPart = (currentPart + 1) % (partsCountSqrt * partsCountSqrt);
        */
    }

    void OnApplicationQuit()
    {
        pIsDone = true;
    }

    public void DeformTerrain(Terrain terrain, Vector3 worldPosition, Texture2D mask, Vector2 finalMaskSize, float heightPerSeconds)
    {
        pEditTerrain(terrain, worldPosition, mask, finalMaskSize, heightPerSeconds, pDeformTerrain);
    }

    public void SmoothTerrain(Terrain terrain, Vector3 worldPosition, Texture2D mask, Vector2 finalMaskSize, float heightPerSeconds)
    {
        pEditTerrain(terrain, worldPosition, mask, finalMaskSize, heightPerSeconds * 0.1f, pSmoothTerrain);
    }

    public void FlattenTerrain(Terrain terrain, Vector3 worldPosition, Texture2D mask, Vector2 finalMaskSize, float heightPerSeconds)
    {
        pEditTerrain(terrain, worldPosition, mask, finalMaskSize, heightPerSeconds, pFlattenTerrain);
    }

    protected void pEditTerrain(Terrain terrain, Vector3 worldPosition, Texture2D mask, Vector2 finalMaskSize, float heightPerSeconds, TerraformAction setHeightAction)
    {
        Vector3 terrainSize = terrain.terrainData.size;
        Vector2 terrainHeightmapSize = new Vector2(terrain.terrainData.heightmapWidth, terrain.terrainData.heightmapHeight);
        Vector2 worldXZPosition = new Vector2(worldPosition.x, worldPosition.z);
        Vector2 terrainXZPosition = new Vector2(terrain.transform.position.x, terrain.transform.position.z);
        Vector2 terrainRelativeXZPosition = worldXZPosition - terrainXZPosition;
        Vector2 finalXZPosition = new Vector2(terrainRelativeXZPosition.x / terrainSize.x * terrainHeightmapSize.x, terrainRelativeXZPosition.y / terrainSize.z * terrainHeightmapSize.y);

        float realPowerRatio = heightPerSeconds * Time.deltaTime / terrainSize.y;

        float[,] newHeights = new float[(int)finalMaskSize.x + 1, (int)finalMaskSize.y + 1];
        float[,,] newAlphamaps = new float[(int)finalMaskSize.x + 1, (int)finalMaskSize.y + 1, terrain.terrainData.alphamapLayers];

        pTerraformData data = new pTerraformData();
        data.terrain = terrain;
        data.mask = mask;
        data.finalMaskSize = finalMaskSize;
        data.finalXZPosition = finalXZPosition;
        data.realPowerRatio = realPowerRatio;

        setHeightAction(data, ref newHeights, ref newAlphamaps);

        pSetTerrainHeight(terrain, finalXZPosition, newHeights, newAlphamaps);
    }

    protected void pUpdateAlphamaps(pTerraformData data, int x, int y, ref float[,,] newAlphamaps)
    {
        //float normX = (data.finalXZPosition.x + x) * 1.0f / (data.terrain.terrainData.heightmapWidth - 1);
        //float normY = (data.finalXZPosition.y + y) * 1.0f / (data.terrain.terrainData.heightmapHeight - 1);

        // Get the steepness value at the normalized coordinate.
        //float angle = data.terrain.terrainData.GetSteepness(normX, normY);
        //float cliffFrac = (angle - (90 - CliffAngle)) / CliffAngle;

        //float height = data.terrain.terrainData.GetHeight((int)(data.finalXZPosition.x + x), (int)(data.finalXZPosition.y + y));

        //float[,,] currentAlpha = data.terrain.terrainData.GetAlphamaps(x, y, 1, 1);
        //float blend = 50.0f * Time.deltaTime;

        /*newAlphamaps[y, x, CliffTextureId] = Mathf.Lerp(currentAlpha[0, 0, CliffTextureId], 1, blend);
        newAlphamaps[y, x, SandTextureId] = Mathf.Lerp(currentAlpha[0, 0, SandTextureId], 0, blend);
        newAlphamaps[y, x, GrassTextureId] = Mathf.Lerp(currentAlpha[0, 0, GrassTextureId], 0, blend);*/
        newAlphamaps[y, x, CliffTextureId] = 0;
        newAlphamaps[y, x, SandTextureId] = 1;
        newAlphamaps[y, x, GrassTextureId] = 0;
    }

    protected void pDeformTerrain(pTerraformData data, ref float[,] newHeights, ref float[,,] newAlphamaps)
    {
        float[,] currentHeight = data.terrain.terrainData.GetHeights((int)data.finalXZPosition.x, (int)data.finalXZPosition.y, newHeights.GetLength(0), newHeights.GetLength(1));

        for (int y = 0; y < newHeights.GetLength(1); y++)
        {
            for (int x = 0; x < newHeights.GetLength(0); x++)
            {
                newHeights[x, y] = currentHeight[x, y] + data.mask.GetPixelBilinear((float)x / data.finalMaskSize.x, (float)y / data.finalMaskSize.y).a * data.realPowerRatio;
                pUpdateAlphamaps(data, x, y, ref newAlphamaps);
            }
        }
    }

    // TODO: real smoothing which keep the slopes and actually smooth (instead of flatten)
    // with for instance subsample 4x4 blocks
    protected void pSmoothTerrain(pTerraformData data, ref float[,] newHeights, ref float[, ,] newAlphamaps)
    {
        pFlattenTerrain(data, ref newHeights, ref newAlphamaps);
    }

    protected void pFlattenTerrain(pTerraformData data, ref float[,] newHeights, ref float[, ,] newAlphamaps)
    {
        float[,] currentHeight = data.terrain.terrainData.GetHeights((int)data.finalXZPosition.x, (int)data.finalXZPosition.y, newHeights.GetLength(0), newHeights.GetLength(1));
        float meanHeight = 0;

        for (int y = 0; y < currentHeight.GetLength(1); y++)
        {
            for (int x = 0; x < currentHeight.GetLength(0); x++)
            {
                meanHeight += currentHeight[x, y];
                pUpdateAlphamaps(data, x, y, ref newAlphamaps);
            }
        }
        meanHeight /= currentHeight.GetLength(0) * currentHeight.GetLength(1);

        for (int y = 0; y < newHeights.GetLength(1); y++)
        {
            for (int x = 0; x < newHeights.GetLength(0); x++)
            {
                float blend = data.mask.GetPixelBilinear((float)x / data.finalMaskSize.x, (float)y / data.finalMaskSize.y).a;
                float current = currentHeight[x, y];
                float newHeight = Mathf.Lerp(current, meanHeight, blend * Time.deltaTime);

                float diff = current - newHeight;
                if (Mathf.Abs(diff) > data.realPowerRatio)
                {
                    newHeight = current + data.realPowerRatio * Mathf.Sign(diff) * blend;
                }
                newHeights[x, y] = newHeight;
                pUpdateAlphamaps(data, x, y, ref newAlphamaps);
            }
        }
    }

    protected void pSetTerrainHeight(Terrain terrain, Vector2 terrainPosition, float[,] heights, float[,,] alphamaps)
    {
        terrain.terrainData.SetHeights((int)terrainPosition.x, (int)terrainPosition.y, heights);
        terrain.terrainData.SetAlphamaps((int)terrainPosition.x, (int)terrainPosition.y, alphamaps);
    }
}

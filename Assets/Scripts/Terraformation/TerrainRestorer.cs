﻿using UnityEngine;
using System.Collections;

public class TerrainRestorer : MonoBehaviour
{
    protected struct s_backedData
    {
        public float[,] heights;
        public float[,,] alphamaps;
    };

    protected Terrain[] pTerrains;
    protected s_backedData[] SavedTerrainData;

    void Awake()
    {
        pTerrains = transform.GetComponentsInChildren<Terrain>(true);
        SavedTerrainData = new s_backedData[pTerrains.Length];
        for (int i = 0; i < pTerrains.Length; i++)
        {
            SavedTerrainData[i] = new s_backedData();
            SavedTerrainData[i].heights = pTerrains[i].terrainData.GetHeights(0, 0, pTerrains[i].terrainData.heightmapWidth, pTerrains[i].terrainData.heightmapHeight);
            SavedTerrainData[i].alphamaps = pTerrains[i].terrainData.GetAlphamaps(0, 0, pTerrains[i].terrainData.alphamapWidth, pTerrains[i].terrainData.alphamapHeight);
        }
    }

    void OnApplicationQuit()
    {
        for (int i = 0; i < pTerrains.Length; i++)
        {
            pTerrains[i].terrainData.SetHeights(0, 0, SavedTerrainData[i].heights);
            pTerrains[i].terrainData.SetAlphamaps(0, 0, SavedTerrainData[i].alphamaps);
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public class GroundCursor : MonoBehaviour
{
    public Collider[] Colliders { get { return pColliders.ToArray(); } }
    public Vector3 Position { get { return transform.position; } }

    protected List<Collider> pColliders;

    void Start()
    {
        pColliders = new List<Collider>();
    }

    void LateUpdate()
    {
        bool shouldHide = true;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(ray, 1024);

        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.gameObject.GetComponent<GroundCursor>() != null)
            {
                continue;
            }

            renderer.enabled = true;
            transform.position = Vector3.Lerp(transform.position, hit.point, 32.0f * Time.deltaTime);
            transform.up = Vector3.Lerp(transform.up, hit.normal, 16.0f * Time.deltaTime);
            shouldHide = false;
            break;
        }

        if (shouldHide)
        {
            renderer.enabled = true;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        pColliders.Add(collider);
    }

    void OnTriggerStay(Collider collider)
    {
    }

    void OnTriggerExit(Collider collider)
    {
        pColliders.Remove(collider);
    }
}

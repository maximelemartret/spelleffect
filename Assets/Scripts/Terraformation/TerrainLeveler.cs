﻿using UnityEngine;
using System.Collections.Generic;

public class TerrainLeveler : MonoBehaviour
{
    public float DefaultMinHeight = 10.0f;

    void Start()
    {
        Terrain terrain = GetComponent<Terrain>();
        TerrainData data = terrain.terrainData;
        float[,] heights = data.GetHeights(0, 0, data.heightmapWidth, data.heightmapHeight);
        DeepSand sand = GameObject.FindObjectOfType<DeepSand>();
        float minLevel = DefaultMinHeight;
        if (sand != null)
        {
            minLevel = sand.DeepSandLevel;
        }

        for (int y = 0; y < data.heightmapHeight; y++)
        {
            for (int x = 0; x < data.heightmapWidth; x++)
            {
                if (x < 5 || x > data.heightmapWidth - 5 || y < 5 || y > data.heightmapHeight - 5 || heights[x, y] < minLevel / data.size.y)
                {
                    heights[x, y] = minLevel / data.size.y;
                }
            }
        }

        data.SetHeights(0, 0, heights);
    }
    
    void Update()
    {
    }
}

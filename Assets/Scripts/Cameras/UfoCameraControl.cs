﻿using UnityEngine;
using System.Collections;

public class UfoCameraControl : MonoBehaviour 
{
    public float Speed = 32.0f;
    public float Sensitivity = 400.0f;

    void Start()
    {
    }
    
    void Update()
    {
        #region Translation
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float elevation = Input.GetAxis("Elevation");
        bool sprint = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        float speed = Speed;
        if (sprint)
        {
            speed *= 4;
        }

        Vector3 forward = transform.forward;
        Vector3 planForward = new Vector3(forward.x, 0, forward.z);
        Vector3 right = transform.right;
        Vector3 move = ((planForward * vertical) + (right * horizontal) + new Vector3(0, elevation, 0)).normalized;

        transform.Translate(move * Time.deltaTime * speed, Space.World);
        #endregion

        #region Rotation
        bool rotateView = Input.GetButton("Rotate View");

        if (rotateView)
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            Vector3 pitch = new Vector3(-mouseY, 0, 0);
            Vector3 yaw = new Vector3(0, mouseX, 0);

            transform.Rotate(pitch * Time.deltaTime * Sensitivity, Space.Self);
            transform.Rotate(yaw * Time.deltaTime * Sensitivity, Space.World);
        }
        #endregion
    }
}

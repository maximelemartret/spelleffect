﻿using UnityEngine;
using System.Collections.Generic;

public class SpellManager : MonoBehaviour
{
    [HideInInspector]
    public List<Spell> SpellBook;
    public Spell ActiveSpell = null;

    protected Spell pActiveSpell;
    protected GroundCursor pCursor;
    protected bool pCanUseSpell;
    protected bool pIsUsingSpell;

    void Start()
    {
        Spell[] spells = GetComponents<Spell>();
        foreach (Spell spell in spells)
        {
            if (ActiveSpell == null)
            {
                ActiveSpell = spell;
            }
            SpellBook.Add(spell);
        }
        
        pCursor = FindObjectOfType<GroundCursor>();

        pCanUseSpell = true;
        pIsUsingSpell = false;
    }
    
    void Update()
    {
        if (ActiveSpell != null)
        {
            bool abort = false;
            if (!pIsUsingSpell && pCanUseSpell && Input.GetButtonDown("Click"))
            {
                pIsUsingSpell = true;
                ActiveSpell.BeginUse(pCursor, ref abort);
                if (abort)
                {
                    pCanUseSpell = false;
                }
            }
            else if (pIsUsingSpell && pCanUseSpell && Input.GetButton("Click"))
            {
                ActiveSpell.UpdateUse(pCursor, ref abort);
                if (abort)
                {
                    pCanUseSpell = false;
                }
            }
            else if ((!pCanUseSpell && pIsUsingSpell) || Input.GetButtonUp("Click"))
            {
                if (pIsUsingSpell)
                {
                    ActiveSpell.EndUse(pCursor);
                }
                pCanUseSpell = true;
                pIsUsingSpell = false;
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Spell : MonoBehaviour
{
    public abstract void BeginUse(GroundCursor groundCursor, ref bool abort);
    public abstract void UpdateUse(GroundCursor groundCursor, ref bool abort);
    public abstract void EndUse(GroundCursor groundCursor);
}

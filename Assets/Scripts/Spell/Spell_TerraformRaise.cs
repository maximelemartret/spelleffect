﻿using UnityEngine;
using System.Collections.Generic;

public class Spell_TerraformRaise : Spell
{
    public float HeightPerSeconds = 50;
    public Vector2 FinalSize = new Vector2(16, 16);
    public Terrain TerraformableTerrain = null;

    protected Texture2D pMask;
    protected Vector2 pHalfMaskSize;
    protected TerraformationEngine pTerraformation;

    void Start()
    {
        pTerraformation = FindObjectOfType<TerraformationEngine>();

        pMask = Resources.Load<Texture2D>("Spells/D_Terraformation_Raise");

        pHalfMaskSize = new Vector2(pMask.width / 2, pMask.height / 2);

        if (TerraformableTerrain == null)
        {
            TerraformableTerrain = Terrain.activeTerrain;
        }
    }

    void Update()
    {
    }

    public override void BeginUse(GroundCursor groundCursor, ref bool abort)
    {
    }
    
    public override void UpdateUse(GroundCursor groundCursor, ref bool abort)
    {
        pTerraformation.DeformTerrain(TerraformableTerrain, groundCursor.Position - new Vector3(pHalfMaskSize.x, 0.0f, pHalfMaskSize.y), pMask, new Vector2(16, 16), HeightPerSeconds);
    }

    public override void EndUse(GroundCursor groundCursor)
    {
    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public class DayNightSkybox : MonoBehaviour
{
    public Material DayNightSkyboxMaterial;
    public float MaxFullDayTime = 240.0f;
    public float TimeScale = 1.0f;
    public GameObject Sun;
    public Color DayFogColor = new Color32(152, 177, 185, 255);
    public Color NightFogColor = new Color32(5, 5, 5, 255);
    // moon too

    protected Material pSkyboxMaterial;
    protected Skybox pSkybox;
    protected float pGameTime;
    protected float pHalfMaxDayTime;
    protected float pQuarterMaxDayTime;

    void Start()
    {
        pSkyboxMaterial = new Material(DayNightSkyboxMaterial);

        pSkybox = GetComponent<Skybox>();
        if (pSkybox == null)
        {
            pSkybox = gameObject.AddComponent<Skybox>();
        }
        pSkybox.material = DayNightSkyboxMaterial;

        pGameTime = 0;
        pHalfMaxDayTime = MaxFullDayTime / 2.0f;
        pQuarterMaxDayTime = pHalfMaxDayTime / 2.0f;
    }

    void Update()
    {
        pGameTime += Time.deltaTime * TimeScale;

        Sun.transform.rotation = Quaternion.Euler((pGameTime + pQuarterMaxDayTime) / pHalfMaxDayTime * 180.0f, 0.0f, 0.0f);

        // TODO: the skybox should fully blend to night only when sun is below sea (between 200 and 340) not right at 180 or 360.
        float blend = Mathf.Cos((Sun.transform.rotation.eulerAngles.x - 90) * Mathf.Deg2Rad);
        pSkybox.material.SetFloat("_Blend", 1 - blend);

        RenderSettings.fogColor = Color.Lerp(NightFogColor, DayFogColor, blend);
    }
}

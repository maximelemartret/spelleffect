﻿Shader "Custom/S_DeepSand"
{
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _BumpTex ("Normal (Height)", 2D) = "white" {}
        _Color ("Color", Color) = (1, 1, 1, 1)
        _Shininess ("Shininess", Range(0, 1)) = 0
        _MaxDepth ("Max Depth", float) = -16.0
        _MaxDepthColor ("Max Depth Color", Color) = (0, 0, 0, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        
        CGPROGRAM
        #pragma surface surf BlinnPhongOneSided
        #pragma target 3.0

        #include "../CustomLighting.cginc"

        #include "UnityCG.cginc"
        
        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpTex;
            float2 uv_LightMap;
            float3 worldPos;
        };
        
        sampler2D _MainTex;
        half4 _MainTex_ST;
        sampler2D _BumpTex;
        half4 _BumpTex_ST;
        half4 _Color;
        half _Shininess;
        half _MaxDepth;
        half4 _MaxDepthColor;
        
        void surf (Input IN, inout SurfaceOutput o)
        {
            half blend = clamp((_MaxDepth - IN.worldPos.y + 1) * 10, 0, 10) / 10.0f;
            half4 depthColor = _MaxDepthColor * blend + half4(1, 1, 1, 1) * (1 - blend);
            half4 c = tex2D(_MainTex, IN.worldPos.xz * 0.001f * _MainTex_ST.xy + _MainTex_ST.zw);
            o.Albedo = c.rgb * _Color.rgb * depthColor;
            o.Alpha = c.a * _Color.a;
            o.Normal = UnpackNormal(tex2D(_BumpTex, IN.worldPos.xz * 0.001f * _BumpTex_ST.xy + _BumpTex_ST.zw));
            o.Gloss = o.Alpha;
            o.Specular = _Shininess;
        }
        ENDCG
    } 
    FallBack "Diffuse"
}

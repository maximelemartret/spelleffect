﻿using UnityEngine;
using System.Collections.Generic;

public class DeepSand : MonoBehaviour
{
    public float DeepSandLevel = 50.0f;

    protected Vector3 pOffset;
    //protected MeshRenderer pMeshRenderer;

    void Start()
    {
        //pMeshRenderer = GetComponent<MeshRenderer>();
        pOffset = new Vector3(transform.position.x, 0, transform.position.z);
    }

    void Update()
    {
        Transform targetTransform = Camera.main.transform;

        transform.position = new Vector3(targetTransform.position.x, DeepSandLevel, targetTransform.position.z);
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, targetTransform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
        transform.Translate(pOffset, Space.Self);

        /*Vector2 seaPlaneWave = new Vector2(Mathf.Cos(Time.time), Mathf.Sin(Time.time) * 0.5f) * 0.001f; // very small
        pMeshRenderer.materials[0].SetTextureOffset("_MainTex", seaPlaneWave);
        pMeshRenderer.materials[0].SetTextureOffset("_DispTex", seaPlaneWave);
        pMeshRenderer.materials[0].SetTextureOffset("_BumpTex", seaPlaneWave);*/
    }
}

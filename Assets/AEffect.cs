﻿using UnityEngine;
using System.Collections;

public abstract class AEffect : MonoBehaviour {

	// Use this for initialization

	public bool tickable;
	public float tick;
	public float chrono;
	public float duration;

	protected abstract void OnApply();
	protected abstract void OnDispell();
	protected abstract void OnTick();
	protected void OnUpdate()
	{
		if (tickable)
		{
			chrono += Time.deltaTime;
			if (chrono >= tick)
			{
				OnTick ();
				chrono -= tick;
			}
		}
		duration -= Time.deltaTime;
		
		if (duration <= 0.0f)
			Destroy (this);
	}

}

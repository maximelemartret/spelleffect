﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class GameObject_WaterMap
{
    [MenuItem("GameObject/Create Other/Water Map")]
    static void createGameObject_WaterMap()
    {
        GameObject obj = new GameObject();

        obj.name = "Water Map";
        obj.AddComponent<WaterMap>();
    }
}

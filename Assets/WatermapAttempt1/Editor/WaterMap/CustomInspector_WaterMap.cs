﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(WaterMap))]
public class CustomInspector_WaterMap : Editor
{
    public override void OnInspectorGUI()
    {
        bool edited = false;
        WaterMap watermap = target as WaterMap;

        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Resolution");
        int resX = EditorGUILayout.IntField("x : ", (int)watermap.MapResolution.x);
        int resY = EditorGUILayout.IntField("y : ", (int)watermap.MapResolution.y);
        if (resX < 2)
        {
            resX = (int)watermap.MapResolution.x;
        }
        if (resY < 2)
        {
            resY = (int)watermap.MapResolution.y;
        }
        if (resX != watermap.MapResolution.x)
        {
            watermap.MapResolution.x = resX;
            edited = true;
        }
        if (resY != watermap.MapResolution.y)
        {
            watermap.MapResolution.y = resY;
            edited = true;
        }
        EditorGUILayout.EndVertical();

        if (edited || watermap.GetComponent<MeshFilter>().mesh == null)
        {
            watermap.UpdateMesh();
        }
    }
}

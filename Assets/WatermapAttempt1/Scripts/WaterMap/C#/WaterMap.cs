﻿using UnityEngine;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class WaterMap : MonoBehaviour
{
    public Vector2 MapResolution = new Vector2(2, 2);

    protected void pGenerateMesh(Vector2 resolution)
    {   
        float ratioX = 1.0f / (resolution.x - 1);
        float ratioY = 1.0f / (resolution.y - 1);
        int squareCount = (int)((resolution.x - 1) * (resolution.y - 1));

        Vector3[] vertices = new Vector3[(int)(resolution.x * resolution.y)];
        int[] indices = new int[(int)(squareCount * 6)];
        Vector2[] uvs = new Vector2[vertices.Length];
        Vector4[] tangents = new Vector4[vertices.Length];

        int verticeIt = 0;
        for (float y = 0; y < resolution.y; y++)
        {
            for (float x = 0; x < resolution.x; x++)
            {
                vertices[verticeIt] = new Vector3(x * ratioX, 0, y * ratioY);
                uvs[verticeIt] = new Vector2(x * ratioX, y * ratioY);
                tangents[verticeIt] = new Vector4(-1.0f, 0.0f, 0.0f, -1.0f);
                verticeIt++;
            }
        }

        int indiceIt = 0;
        int it = 0;
        for (int square = 0; square < squareCount; square++)
        {
            // lower triangle
            indices[it] = indiceIt + (int)(resolution.x);
            indices[it + 1] = indiceIt + 1;
            indices[it + 2] = indiceIt;
            // higher triangle
            indices[it + 3] = indiceIt + (int)(resolution.x) + 1;
            indices[it + 4] = indiceIt + 1;
            indices[it + 5] = indiceIt + (int)(resolution.x);
            it += 6;
            indiceIt++;
            if (indiceIt > 0 && (indiceIt + 1) % resolution.x == 0)
            {
                indiceIt++;
            }
        }

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = indices;
        mesh.tangents = tangents;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    public void UpdateMesh()
    {
        if (MapResolution.x < 2)
        {
            MapResolution.x = 2;
        }

        if (MapResolution.y < 2)
        {
            MapResolution.y = 2;
        }

        pGenerateMesh(MapResolution);
    }

	void Start()
    {
        if (GetComponent<MeshFilter>().mesh == null)
        {
            GetComponent<MeshFilter>().mesh = new Mesh();
        }
        UpdateMesh();
	}
}

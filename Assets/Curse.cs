﻿using UnityEngine;
using System.Collections;

public class Curse : AEffect {

	bool first;

	// Use this for initialization
	void Start ()
	{
		duration = 120.0f;
		tick = 0.0f;
		tickable = false;
		chrono = 0.0f;
		first = true;
	}
	
	protected override void OnApply()
	{
		GetComponent<LivingUnit>().def -= 20;
		Debug.Log ("Effect applied : Curse.");
		first = false;
	}
	
	protected override void OnDispell()
	{
		GetComponent<LivingUnit>().def += 20;
		Debug.Log ("Effect removed : Curse.");
	}
	
	protected override void OnTick()
	{
	}
	
	void onDestroy()
	{
		OnDispell ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (first)
			OnApply ();
		OnUpdate ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class Fortify : AEffect {

	bool first;

	// Use this for initialization
	void Start ()
	{
		duration = 300.0f;
		tick = 0.0f;
		tickable = false;
		chrono = 0.0f;
		first = true;
	}
	
	protected override void OnApply()
	{
		GetComponent<LivingUnit>().def += 25;
		Debug.Log ("Effect applied : Fortify.");
		first = false;
	}
	
	protected override void OnDispell()
	{
		GetComponent<LivingUnit>().def -= 25;
		Debug.Log ("Effect removed : Fortify.");
	}
	
	protected override void OnTick()
	{
	}
	
	void onDestroy()
	{
		OnDispell ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (first)
			OnApply ();
		OnUpdate ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class Courage : AEffect {

	bool first;

	// Use this for initialization
	void Start ()
	{
		duration = 30.0f;
		tick = 0.0f;
		tickable = false;
		chrono = 0.0f;
		first = true;
	}
	
	protected override void OnApply()
	{
		GetComponent<LivingUnit>().atk += 30;
		Debug.Log ("Effect applied : Courage.");
		first = false;
	}
	
	protected override void OnDispell()
	{
		GetComponent<LivingUnit>().atk -= 30;
		Debug.Log ("Effect removed : Courage.");
	}
	
	protected override void OnTick()
	{
	}
	
	void onDestroy()
	{
		OnDispell ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (first)
			OnApply ();
		OnUpdate ();
	}
}
